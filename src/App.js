import React, { Component } from "react"
import "./App.css"
import Cards from "./Components/Cards"
import CardDetails from "./Components/CardDetails"
import Search from "./Components/Search"
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import { Router, Route, browserHistory } from "react-router"

class App extends Component {
  render() {
    return (
      <div className="App container">
        <Search />
        <Router history={browserHistory}>
          <Route path="/" component={Cards} />
          <Route path="/details/:id" component={CardDetails} />
        </Router>
      </div>
    )
  }
}

export default App
