import { extendObservable } from "mobx"
import axios from "axios"
import configAPI from "../config.js"

class Main {
  constructor() {
    extendObservable(this, {
      cardsData: [],
      showViral: true,
      section: "hot",
      sort: "viral",
      window: "day",
      loader: false,
      cardDetails: {},
      images: []
    })
  }

  getDataFromService() {
    const { section, sort, window, showViral } = this
    this.loader = true
    const URL = `https://api.imgur.com/3/gallery/${section}/${sort}/${window}&showViral=${showViral}`
    const headers = { Authorization: `Client-ID ${configAPI.APIKEY}` }
    const config = { headers: headers }
    return axios.get(URL, config).then(res => {
      this.cardsData = res.data.data
      this.loader = false
    })
  }

  handleChangeSection(event) {
    this.section = event.target.value
  }

  handleChangeSort(event) {
    this.sort = event.target.value
  }

  handleChangeViral(event) {
    this.showViral = !this.showViral
  }
}

export default new Main()
