import React, { Component } from "react"
import "rc-tooltip/assets/bootstrap_white.css"
import { observer, inject } from "mobx-react"
import FontAwesome from "react-fontawesome"
import { browserHistory } from "react-router"

class CardDetails extends Component {
  componentWillMount() {
    window.scrollTo(0, 0)
    if(!this.props.Main.cardDetails.title){
      browserHistory.push('/')
    }
  }
  render() {
    const { cardDetails, images } = this.props.Main
    const Image = () => (
      <div>
        <img src={cardDetails.link} className="img-responsive" alt={cardDetails.description}/>
      </div>
    )
    const Images = () => (
      <div>
        {images.map(image => (
          <p>
            <img src={image.link} className="img-responsive" alt={cardDetails.description}/>
          </p>
        ))}
      </div>
    )

    return (
      <section className="CardDetails">
      <div>
        <h3>{cardDetails.title}</h3>
        <h4>
          by <span>{cardDetails.account_url}</span>
        </h4>
        <h5>{cardDetails.description}</h5>
        </div>
        <div>
          <FontAwesome
            className="super-crazy-colors"
            name="thumbs-up"
            size="1px"
            style={{ textShadow: "0 1px 0 rgba(0, 0, 0, 0.1)" }}
          />{" "}
          {cardDetails.ups}
          <FontAwesome
            className="super-crazy-colors"
            name="thumbs-down"
            size="1px"
            style={{ textShadow: "0 1px 0 rgba(0, 0, 0, 0.1)", marginLeft: 7 }}
          />{" "}
          {cardDetails.downs}
          <FontAwesome
            className="super-crazy-colors"
            name="star"
            size="1px"
            style={{ textShadow: "0 1px 0 rgba(0, 0, 0, 0.1)", marginLeft: 7 }}
          />{" "}
          {cardDetails.score} points
        </div>
        {cardDetails.images ? <Images /> : <Image />}
      </section>
    )
  }
}

export default inject("Main")(observer(CardDetails))
