import React, { Component } from "react"
import "../App.css"
import logo from "../Images/logo.png"
import { observer, inject } from "mobx-react"
import { browserHistory } from "react-router"

class Search extends Component {
  constructor() {
    super()
    this.handleChangeSection = this.handleChangeSection.bind(this)
    this.handleChangeSort = this.handleChangeSort.bind(this)
    this.handleChangeViral = this.handleChangeViral.bind(this)
    this.handleChangeWindow = this.handleChangeWindow.bind(this)
  }

  actionAfterChange(){
    this.props.Main.getDataFromService()
    browserHistory.push('/')
  }
  handleChangeSection(event) {
    this.props.Main.section = event.target.value
    this.actionAfterChange()
  }

  handleChangeSort(event) {
    this.props.Main.sort = event.target.value
    this.actionAfterChange()
  }

  handleChangeViral(event) {
    this.props.Main.showViral = !this.props.Main.showViral
    this.actionAfterChange()
  }

  handleChangeWindow(event) {
    this.props.Main.window = event.target.value
    this.actionAfterChange()
  }

  render() {
    const { section, sort, showViral, window, loader } = this.props.Main
    return (
      <section className="Search">
        <div className="form-inline">
          <img src={logo} height={50} alt={"Mobilabs logo"}/>
          <div className="form-group">
            <label>Section</label>
            <select
              value={section}
              className="form-control"
              onChange={this.handleChangeSection}
            >
              <option value="hot">HOT</option>
              <option value="top">TOP</option>
              <option value="user">USER</option>
            </select>
          </div>
          {section === "user" && (
            <div className="form-group">
              <div className="checkbox">
                <label>
                  <input
                    type="checkbox"
                    checked={showViral}
                    onChange={this.handleChangeViral}
                  />Show Viral?
                </label>
              </div>
            </div>
          )}
          <div className="form-group">
            <label>Sort</label>
            <select
              value={sort}
              className="form-control"
              onChange={this.handleChangeSort}
            >
              <option value="viral">VIRAL</option>
              <option value="top">TOP</option>
              <option value="time">TIME</option>
              <option value="rising">RISING</option>
            </select>
          </div>
          <div className="form-group">
            <label>Window</label>
            <select
              value={window}
              className="form-control"
              onChange={this.handleChangeWindow}
            >
              <option value="day">DAY</option>
              <option value="week">WEEK</option>
              <option value="month">MONTH</option>
              <option value="year">YEAR</option>
              <option value="all">ALL</option>
            </select>
          </div>
          <div className="form-group text-center">
            {loader && <div className="loader" />}
          </div>
        </div>
      </section>
    )
  }
}

export default inject("Main")(observer(Search))
