import React, { Component } from "react"
import axios from "axios"
import Tooltip from "rc-tooltip"
import configAPI from "../config.js"
import "rc-tooltip/assets/bootstrap_white.css"
import { observer, inject } from "mobx-react"
import { browserHistory } from "react-router"

class Cards extends Component {
  constructor() {
    super()
    this.getDetails = this.getDetails.bind(this)
  }
  componentWillMount() {
    this.props.Main.getDataFromService(
      this.props.Main.section,
      this.props.Main.sort
    )
  }

  getDataFromService(section, sort) {
    const URL = `https://api.imgur.com/3/gallery/${section}/${sort}`
    const headers = { Authorization: `Client-ID ${configAPI.APIKEY}` }
    const config = { headers: headers }
    const self = this
    return axios
      .get(URL, config)
      .then(res => self.setState({ cardsData: res.data.data }))
  }

  getDetails(card) {
    this.props.Main.cardDetails = card
    this.props.Main.images = card.images
    browserHistory.push(`/details/${card.id}`)
  }

  render() {
    const { cardsData } = this.props.Main
    return (
      <section className="Cards">
        <div className="row">
          {cardsData
            .filter(
              item =>
                item.link.includes(".png") ||
                item.link.includes(".jpg") ||
                item.link.includes(".gif")
            )
            .map((card, index) => (
              <div
                className="col-sm-3"
                onClick={() => this.getDetails(card)}
                key={index}
              >
                <Tooltip
                  placement="left"
                  overlay={
                    <p className="truncate">
                      {card.description
                        ? card.description
                        : "No description"}
                      <br />
                      {card.points} points
                    </p>
                  }
                  arrowContent={<div className="rc-tooltip-arrow-inner" />}
                >
                  <div
                    className="Content"
                    style={{
                      backgroundImage: `url(${card.link})`
                    }}
                  />
                </Tooltip>
              </div>
            ))}
          {cardsData.filter(item => item.images).map((card, index) => (
            <div
              className="col-sm-3"
              onClick={() => this.getDetails(card)}
              key={index}
            >
              <Tooltip
                placement="left"
                overlay={
                  <p className="truncate">
                    {card.description
                      ? card.description
                      : "No description"}
                    <br />
                    {card.points} points
                  </p>
                }
                arrowContent={<div className="rc-tooltip-arrow-inner" />}
              >
                <div
                  className="Content"
                  style={{
                    backgroundImage: `url(${card.images[0].link})`
                  }}
                />
              </Tooltip>
            </div>
          ))}
        </div>
      </section>
    )
  }
}

export default inject("Main")(observer(Cards))
